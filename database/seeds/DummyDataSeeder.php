<?php

use Illuminate\Database\Seeder;
use App\Customer;
use App\Transaction;

/**
 * Class DummyDataSeeder
 */
class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = new Customer();
        $customer->name = 'John Dou';
        $customer->cnp = 123456789;

        $customer->save();

        $transaction = new Transaction();
        $transaction->customer_id = Customer::where('name', 'John Dou')->first()->id;
        $transaction->amount = 50.1;
        $transaction->save();

        $transaction = new Transaction();
        $transaction->customer_id = Customer::where('name', 'John Dou')->first()->id;
        $transaction->amount = 100.8;
        $transaction->save();
    }
}
