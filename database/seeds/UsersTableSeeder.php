<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('username', 'admin')->first()) {
            $user = new User();
            $user->username = 'admin';
            $user->password = Hash::make('admin');
            $user->save();
        }
    }
}
