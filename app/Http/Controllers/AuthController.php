<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthController
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function login(Request $request)
    {
        return view('auth/login');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     *
     * @throws \Illuminate\Validation\ValidationException
     *
     */
    public function loginCheck(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = User::where('username', $request->input('username'))->first();

        if (!$user or !Hash::check($request->input('password'), $user->password)) {
            return response()->json([
                'success' => false,
                'message' => 'wrong credentials',
            ]);
        }

        $token = sha1(str_random(40));
        User::where('id', $user->id)->update(['api_token' => $token]);

        return response()->json([
            'success' => true,
            'api_token' => $token,
        ]);
    }
}