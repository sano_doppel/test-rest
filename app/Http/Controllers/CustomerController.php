<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

/**
 * Class CustomerController
 */
class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cnp' => 'required|regex:/[0-9]/',
        ]);

        $customer = new Customer;
        $customer->name = $request->name;
        $customer->cnp = $request->cnp;
        $customer->save();

        return response()->json(
            [
                'customerId' => $customer->id,
            ]
        );
    }
}