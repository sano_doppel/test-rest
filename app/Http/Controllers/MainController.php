<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        return view('index', ['apiToken' => $request->get('api_token')]);
    }
}
