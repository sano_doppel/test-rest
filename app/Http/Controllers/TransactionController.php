<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

/**
 * Class TransactionController
 */
class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'customerId' => 'integer|gt:0',
            'amount' => 'numeric',
            'date' => 'date',
            'offset' => 'integer',
            'limit' => 'integer|gt:0',
        ]);

        return response()->json(
            Transaction::getByFilters($request->all())
        );
    }

    /**
     * @param Request $request
     * @param integer $customerId
     * @param integer $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $customerId, $id)
    {
        return response()->json(
            Transaction::where('customer_id', $customerId)
                ->where('id', $id)
                ->first()
        );
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'customerId' => 'required|integer|gt:0|exists:customers,id',
            'amount' => 'required|numeric',
        ]);

        $transaction = new Transaction();
        $transaction->customer_id = $request->customerId;
        $transaction->amount = $request->amount;
        $transaction->save();

        return response()->json(
            $transaction
        );
    }

    /**
     * @param Request $request
     *
     * @param integer $id
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'amount' => 'required|numeric',
        ]);

        $transaction = Transaction::where('id', $id)->first();

        if ($transaction) {
            $transaction->amount = $request->amount;
            $transaction->save();
        }

        return response()->json(
            $transaction
        );
    }

    /**
     * @param Request $request
     * @param integer $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        return response()->json(['status' => Transaction::destroy($id) ? 'success': 'fail']);
    }
}