<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 */
class Transaction extends Model
{
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get transactions by filters
     *
     * @param $filters
     *
     * @return mixed
     */
    public static function getByFilters($filters)
    {
        $where = [];
        if (isset($filters['amount'])) {
            $where[] = ['amount', '=', $filters['amount']];
        }
        if (isset($filters['date'])) {
            $where[] = ['created_at', '>=', $filters['date']];
        }
        if (isset($filters['customerId'])) {
            $where[] = ['customer_id', '=', $filters['customerId']];
        }

        $transactions = static::where($where)
            ->orderBy('updated_at', 'desc');

        if (isset($filters['limit'])) {
            $transactions = $transactions->take($filters['limit']);
        }
        if (isset($filters['offset'])) {
            $transactions = $transactions->offset($filters['offset']);
        }

        return $transactions->get();
    }
}
