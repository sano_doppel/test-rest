<?php
namespace App\Console\Commands;

use App\Transaction;
use App\TransactionSum;
use Illuminate\Console\Command;

/**
 * Class TransactionDaySumCommand
 */
class TransactionDaySumCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "transaction_sum:day";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Stores the sum of all transactions from previous day";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $day = date('Y-m-d');
        $sumEntity = TransactionSum::where('day', $day)->first();

        $amount = Transaction::selectRaw('sum(amount) as sum')
            ->where([
                ['created_at', '>=', $day],
                ['created_at', '<', date('Y-m-d',strtotime($day . '+1 days'))],
            ])
            ->groupBy()
            ->first();
        $sum = $amount ? $amount->sum : 0;

        if (!$sumEntity) {
            $sumEntity = new TransactionSum();
            $sumEntity->day = $day;
        }

        $sumEntity->amount = $sum;
        $sumEntity->save();

        echo 'DONE'.PHP_EOL;
        return true;
    }
}