<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login page</title>
</head>
<body>
<h1>Login page</h1>

<p>For this test case use</br>
    login: <i>admin</i> </br>
    password: <i>admin</i>
</p>

<form action="/" method="POST" id="theForm" onsubmit="login(this)">
    <p>
        <label for="auth-username">Username:</label> <input type="text" id="auth-username" name="username">
    </p>
    <p>
        <label for="auth-password">Password:</label> <input type="password" id="auth-password" name="password">
    </p>
    <p>
        <input type="submit" value="Submit">
    </p>
        <input type="hidden" id="auth-api_token" name="api_token" value="">
</form>
</body>

<script>
    function login(form) {
        event.preventDefault();
        let xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                let response = JSON.parse(xhttp.response);
                if (!response.success) {
                    alert(response.message);
                } else {
                    document.getElementById("auth-api_token").setAttribute('value', response.api_token);
                    document.getElementById("auth-password").setAttribute('value', '');
                    form.submit();
                }
            }
        };

        xhttp.open("POST", "/auth", true);
        xhttp.send(new FormData(form));
    }
</script>
</html>