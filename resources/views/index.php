<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Transaction list</title>
</head>
<body>
    <h1>Transaction list</h1>
    <table id="transactions" border="1">
        <tr>
            <th>ID</th>
            <th>Amount</th>
            <th>Customer ID</th>
            <th>Created at</th>
            <th>Updated at</th>
        </tr>
    </table>
    <script>
        let xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                let response = JSON.parse(xhttp.response);
                let table = document.getElementById("transactions");
                for (let key in response) {
                    let row = table.insertRow(parseInt(key)+1),
                    id = row.insertCell(0),
                    amount = row.insertCell(1),
                    customerId = row.insertCell(2),
                    createdAt = row.insertCell(3),
                    updatedAt = row.insertCell(4);

                    id.innerHTML = response[key]['id'];
                    amount.innerHTML = response[key]['amount'];
                    customerId.innerHTML = response[key]['customer_id'];
                    createdAt.innerHTML = response[key]['created_at'];
                    updatedAt.innerHTML = response[key]['updated_at'];
                }

            }
        };

        let params = {
            'api_token': '<?=$apiToken?>',
            'date':'2018-01-01',
            'customerId': 1,
            'limit': 6,
            'offset': 0
        },
        encodedParams = [];

        for (var key in params) {
            encodedParams.push(key + '=' + encodeURIComponent(params[key]));
        }

        xhttp.open("GET", "/transaction?"+encodedParams.join('&'), true);
        xhttp.send();
    </script>
</body>
</html>