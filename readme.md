### Created with Lumen PHP framework 


## install: 
1. ```git clone https://bitbucket.org/sano_doppel/test-rest.git```
2. ```composer install```
3. configure your local environment in .env file.
4. ```php artisan migrate```
5. To create admin use ```php artisan db:seed --class=UsersTableSeeder```
6. To install dummy data exec command ```php artisan db:seed --class=DummyDataSeeder ```






### Cron job to run every 2 days at 23:47: 

```47 23 */2 * *  php artisan transaction_sum:day```



### API CALL Examples: 

##### auth 
curl --header "Content-Type: application/json" --request POST --data '{"username":"<USERNAME>","password":"<PASSWORD>"}' <APP_URL>/auth

##### add customer
curl --header "Content-Type: application/json" --request POST --data '{"api_token":"<TOKEN>","name":"<NAME>","cnp":"<CNP>"}' <APP_URL>/customer

##### get transaction list
curl --header "Content-Type: application/json" --request GET --data '{"api_token":"<TOKEN>"}' <APP_URL>/transaction/

##### get transaction
curl --header "Content-Type: application/json" --request GET --data '{"api_token":"<TOKEN>"}' <APP_URL>/transaction/<CUSTOMER_ID>/<ID>

##### add transaction
curl --header "Content-Type: application/json" --request POST --data '{"api_token":"<TOKEN>","customerId":"<CUSTOMER_ID>","amount":"<AMOUNT>"}' <APP_URL>/transaction

##### update transaction
curl --header "Content-Type: application/json" --request PUT --data '{"api_token":"<TOKEN>","amount":"<AMOUNT>"}' <APP_URL>/transaction/<ID>

##### delete transaction
curl --header "Content-Type: application/json" --request DELETE --data '{"api_token":"<TOKEN>"}' <APP_URL>/transaction/<ID>
