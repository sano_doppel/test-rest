<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', ['middleware' => 'auth', 'uses' => 'MainController@index']);
$router->post('/', ['middleware' => 'auth', 'uses' => 'MainController@index']);


$router->get('/login', 'AuthController@login');
$router->post('/auth', 'AuthController@loginCheck');


$router->post('customer/', 'CustomerController@store');


$router->get('transaction/', 'TransactionController@index');
$router->get('transaction/{customerId:[0-9]+}/{id:[0-9]+}/', 'TransactionController@show');
$router->post('transaction/','TransactionController@store');
$router->put('transaction/{id:[0-9]+}', 'TransactionController@update');
$router->delete('transaction/{id:[0-9]+}', 'TransactionController@destroy');